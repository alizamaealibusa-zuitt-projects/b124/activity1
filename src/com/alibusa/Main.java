package com.alibusa;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Scanner appScanner = new Scanner(System.in);

        String firstName;
        String lastName;
        int englishGrade;
        int mathGrade;
        int scienceGrade;

        System.out.println("What is your first name? ");
        firstName = appScanner.nextLine().trim();

        System.out.println("What is your last name? ");
        lastName = appScanner.nextLine().trim();

        System.out.println("What is grade in English? ");
        englishGrade = appScanner.nextInt();

        System.out.println("What is grade in Math? ");
        mathGrade = appScanner.nextInt();

        System.out.println("What is grade in Science? ");
        scienceGrade = appScanner.nextInt();

        // Computing the Average Grade
        int averageGrade = ((englishGrade + mathGrade + scienceGrade) / 3);

        System.out.println("Hi, " + firstName + " " + lastName + "! Your average grade is " + averageGrade + ".");
    }
}
